package com.company;

public class CalculatorFactory
{
    public static Calculator getCalculator(int choice)
    {
        if(choice==1)
            return new NormalCalculator();
        else if(choice==2)
            return new MagicCalculator();
        return null;
    }
}
