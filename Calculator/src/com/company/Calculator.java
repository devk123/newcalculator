package com.company;

public interface Calculator
{
    Chipset chipset=new S9412AChipset();

    int sum(int a[],int b);
    int subtract(int a[],int b);

}
